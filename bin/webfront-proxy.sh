#! /bin/bash

NAME=$1
IP=$2
PORT=$3
UPSTREAM=$NAME-$PORT


cat <<HERE
upstream $UPSTREAM {
	server $IP:$PORT;
}

server {

	listen [::]:$PORT ssl;
	listen $PORT ssl;

	server_name localhost;

	# certs
	ssl_certificate /var/webfront/ssl/default.pem;
	ssl_certificate_key /var/webfront/ssl/default.pem;

## oscp stapling is futile with self signed certificates since there is no oscp resolver
#	ssl_trusted_certificate /etc/nginx/ssl/opendatacity.de.stapling;
#	ssl_stapling on;
#	ssl_stapling_verify on;

	# add hsts
	add_header Strict-Transport-Security max-age=15552000;

	# include global cipher config
	# include ssl/ssl.conf;

	keepalive_timeout 70;
	spdy_headers_comp 6;
	
	root /var/webfront/html/default;

	index index.html index.htm;

	location / {
		proxy_redirect off;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		proxy_set_header Host $host;
		proxy_set_header X-Original-IP $remote_addr;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_redirect off;

		proxy_connect_timeout 159s;
		proxy_send_timeout   600;
		proxy_read_timeout   600;
		proxy_buffer_size    64k;
		proxy_buffers     16 32k;
		proxy_busy_buffers_size 64k;
		proxy_temp_file_write_size 64k;

		client_max_body_size 50m;

		client_body_buffer_size 50m;
		proxy_pass http://$UPSTREAM;
		auth_basic            "$UPSTREAM";
		auth_basic_user_file  /var/webfront/.htpasswd;
	}
HERE
