#! /bin/bash

NAME=couchdb

IP=$(docker inspect --format="{{.NetworkSettings.IPAddress}}" $NAME)

SERVICE=couchdb

if [ -n "$IP" ] ; then 
 ~/bin/webfront-proxy $SERVICE $IP 5984 >/config/webfront/$SERVICE.conf
else 
  echo No IP for $NAME 
fi
