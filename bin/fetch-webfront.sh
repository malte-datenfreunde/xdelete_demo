#! /bin/bash
rsync -av demeter:/home/core/unitfiles/webfront.service .
rsync -av demeter:/home/core/unitfiles/influxdb.service .
rsync -av demeter:/home/core/unitfiles/couchdb.service .
rsync -av demeter:/var/webfront webfront/var/
rsync -av demeter:/config/webfront webfront/config/
rsync -av demeter:/home/core/bin/ ./bin/
