#! /bin/bash

export PATH=$PATH:/home/core/bin

NAME=influxdb.service

IP=$(docker inspect --format="{{.NetworkSettings.IPAddress}}" $NAME)

SERVICE=$NAME-admin

webfront-proxy $SERVICE $IP 8083 >/config/webfront/$SERVICE.conf

SERVICE=$NAME-data

webfront-proxy $SERVICE $IP 8086 >/config/webfront/$SERVICE.conf

exit 0
