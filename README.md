# demeter.xmnt.co

This will be our throw-away host for the next two weeks. Things we build here
have to be repeatable on any other coreos machine and backed up somewhere
secure, but setup does not have to be automated.

The following services are kept up running by [me], but if you have to mess with it, here
is the lowdown:

## webfront

Controlled by the unitfile /home/core/unitfiles/webfront.service, is a nginx in
docker container named webfront.

  - Start: `sudo systemctl start webfront.service

  - Stop:  `sudo systemctl stop webfront.service

  - To add a new static website, you have to create a .conf file in `/config/webfront` and a directory for the files in `/var/webfront/html`, and then do a `sudo systemctl restart webfront`. You can use `/config/webfront/default.conf` and `/var/webfront/html/default` as
  templates for this.

  - Troubleshooting: `systemctl status -l webfront.service | less` gives you the logfiles


## influxdb

Controlled by the unitfile `/home/core/unitfiles/influxdb.service`, is an influxdb in
a docker container behind a webfront.

  - Start: `sudo systemctl start influxdb.service`. Do a `sudo systemctl reload webfront` afterwards to
  configure the webfront automatically.

  - Troubleshooting: `systemctl status -l influxdb.service | less` gives you the logfiles

  - Stop:  `sudo systemctl stop influxdb.service

  - The admin interface is reachable at <https://demeter.xmnt.co:8083>.

  - The REST interface is reachable at <https://demeter.xmnt.co:8086>.

  - HTTP Basic Auth for any non GET HEAD OPTIONS requests

  - official certificate coming soon.

  - Data is stored in `/var/influxdb` and survives a container restart - although it does not seem that way due to this bug: <https://github.com/influxdb/influxdb/issues/3219>


## couchdb

A couchdb container with webfront.

Not yet controlled by a unitfile. Working on it. For now, you have to run manually:

```
docker stop couchdb
docker rm couchdb
docker run --name=couchdb -d -v /var/couchdb:/usr/local/var/lib/couchdb klaemo/couchdb
cd /home/core
bin/setup-couchdbfront.sh
sudo systemctl restart webfront
```

  - The REST interface is reachable at <https://demeter.xmnt.co:5984>.

  - HTTP Basic Auth and official certificate coming soon.

  - Data is stored in `/var/couchdb` and survives a container restart.



[me]: https://twitter.com/mvtango
